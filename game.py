from termcolor import colored
import random, time

WIDTH = 10
HEIGHT = 10
DIRECTIONS = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW']


def row_to_num(row):
    """
    Convert row to num.

    Example: ord('A') = 65, so row = 0
    """
    return ord(row) - 65


class Pos:
    def __init__(self, row: int | str, col):
        if type(row) is str:
            self.row = self.__row_number(row)
        else:
            self.row = row

        self.col = col

    def is_left(self, other: 'Pos'):
        return self.col > other.col

    def is_right(self, other: 'Pos'):
        return self.col < other.col

    def is_up(self, other: 'Pos'):
        return self.row > other.row

    def is_down(self, other: 'Pos'):
        return self.row < other.row

    def is_same_row(self, other: 'Pos'):
        return self.row == other.row

    def is_same_col(self, other: 'Pos'):
        return self.col == other.col

    def __row_letter(self):
        return chr(self.row + 64)

    def __row_number(self, row_letter):
        return ord(row_letter) - 64

    def __iter__(self):
        return iter((self.row, self.col, ))

    def __str__(self):
        return str((self.__row_letter(), self.col, ))

    def __eq__(self, other: 'Pos'):
        return self.row == other.row and self.col == other.col


class Character:
    def __init__(self, board, pos=Pos('A', 1)):
        """TODO."""
        self.pos = pos
        self.board = board

    def move(self, pos: Pos):
        """TODO."""
        self.board.fill_position(self, pos)
        self.pos = pos

    def direction_to(self, character):
        """TODO."""
        # The easy bits
        # in same row
        if self.pos.row == character.pos.row:
            return self.pos.col > character.pos.col and 'E' or 'W'

        # in same column
        if self.pos.col == character.pos.col:
            return self.pos.row > character.pos.row and 'S' or 'N'

    def as_colored(self):
        """TODO."""
        return colored(str(self), self.color)

    def __str__(self):
        print(type(self).__name__)


class BoardError(Exception):
    pass


class Board:
    def __init__(self):
        self.width = WIDTH
        self.height = HEIGHT
        self.board = self._initialize_board(self.width, self.height)

    def _initialize_board(self, width, height):
        board = []
        for h in range(1, height + 1):
            row = []
            for w in range(1, width + 1):
                row.append(None)
            board.append(row)
        return board

    def clear_position(self, character: Character):
        row = character.pos.row - 1
        col = character.pos.col - 1
        self.board[row][col] = None

    def fill_position(self, character: Character, pos: Pos):
        self.clear_position(character)

        row = pos.row - 1
        col = pos.col - 1
        # print("FILL", row+1, col+1, pos)
        self.board[row][col] = character

    def is_legal_move(self, character, new_pos):
        """
        Return whether move is legal.

        A character can move in any direction but in a limited way. If going from
        one to another is too far, it will return False.
        """

        budgeted = (abs(character.pos.row - new_pos.row)) + (abs(character.pos.col - new_pos.col))

        # print(character)
        if (budgeted > character.max_moves):
            # print("Moved too much", new_pos)
            return False

        if board.is_past_edge(new_pos):
            # print("PAST EDGE", new_pos)
            return False

        if board.is_space_occupied(new_pos):
            # print("SPACE OCC", new_pos)
            return False

        # print("OK!", new_pos)
        return True

    def is_past_edge(self, pos):
        """
        Return if past edge of board.

        If one or both coordinates are past a threshold, return True
        """
        return (pos.row > self.width or pos.row <= 0 or
                pos.col > self.height or pos.col <= 0)

    def is_space_occupied(self, pos):
        row = pos.row - 1
        col = pos.col - 1
        # print("ISO CHECK", pos, self.board[row + 1][col + 1])
        if self.board[row][col]:
            return True

    def path_search_space(self, moving_character: Character, moving_to: Pos):
        """
        Return two tuples: row from i to j and col from k to l.

        so: for r in range(i,j+1): for c in range(k,l+1)

        In English :) : If I kick to a square, I will try to find players that
        are one step from my current square to the point where I am kicking to.
        Here is a board:

          1     2     3      4     5     6
         -----|-----|------|------|----|----|
        1|    |     |  x   |  y   |z   |    |
         -----|-----|------|------|----|----|
        2|    |     |  y   |      |t5  |    |
         -----|-----|------|------|----|----|
        3|    | t4  |  y   |  z   |y   |    |
         -----|-----|------|------|----|----|
        4|    |     | t3   |      |t1  |    |
         -----|-----|------|------|----|----|
        5|    |     |      |      |t2  |    |
         -----|-----|------|------|----|----|
        6|    |     |      |      |    |    |
         -----|-----|------|------|----|----|

        If I kick "x" to "t1", the search space would be (x is at 1, 3)
        rows = 2, 3 and 4
        cols = 4 and 5

        If I loop through those (for each row, go through each column)
        I will be able to find players on that board.
        """
        if moving_to.row > WIDTH or moving_to.col > HEIGHT:
            raise BoardError('Out of range')

        if moving_character.pos.is_same_row(moving_to):
            row = (moving_character.pos.row, moving_character.pos.row)
        else:
            row = (moving_character.pos.is_up(moving_to) and
                   (moving_to.row, moving_character.pos.row - 1) or
                   (moving_character.pos.row + 1, moving_to.row))

        if moving_character.pos.is_same_col(moving_to):
            col = (moving_character.pos.col, moving_character.pos.col)
        else:
            col = (moving_character.pos.is_left(moving_to) and
                   (moving_to.col, moving_character.pos.col - 1) or
                   (moving_character.pos.col + 1, moving_to.col))

        return (row, col)

    def path_will_cross_character(self,
                                  moving_character: Character,
                                  moving_to: Pos,
                                  characters: [Character]):
        """
        Return the closest character that another character crosses.

        Get the search space to find players.

        Find all players that are within those two points.

        Then query each one to see if the moving character would be left, right,
        up, down (or diagonal).

        Find the one the moving character would hit first.

        The idea here is to use the search space from `path_search_space()` and
        loop through it, finding players that are closest.
        """
        (row, col) = self.path_search_space(moving_character, moving_to)
        players = []
        for r in range(row[0], row[1] + 1):
            for c in range(col[0], col[1] + 1):
                if self.board[r - 1][c - 1]:
                    players.append(self.board[r - 1][c - 1])

        if moving_character.pos.is_left(moving_to):
            # Why? We implicitly assume that the ones evaluated earlier are closer
            # to the moving object in the search space than those farther away.
            # In this case, we are going backwards, so need to do this implicit
            # check the other way.
            players.reverse()

        if len(players) == 0:
            return None

        smallest_distance = players[0]

        for player in players:
            if moving_character.pos.is_up(moving_to):
                smallest = smallest_distance.pos.row - smallest_distance.pos.col
                curr = player.pos.row - player.pos.col

            else:
                smallest = abs(smallest_distance.pos.col + smallest_distance.pos.row)
                curr = abs(player.pos.col + player.pos.row)

            if smallest > curr:
                smallest_distance = player

        return smallest_distance

    def is_position_valid(self, pos):
        row = pos.row
        col = pos.col - 1
        if row > HEIGHT or col > WIDTH:
            raise BoardError("Bad position")
        if self.board[row][col] is not None:
            raise BoardError("Space occupied")

    def __iter__(self):
        return iter(self.board)


    def __str__(self):
        result = ""
        count = 0
        row_count = 65

        result += " "
        for top in range(1, WIDTH+1):
            result += '   '
            if top >= 10:
                result += f"  {top} "
            else:
                result += f"  {top}  "
        result += "   \n"

        for row in self.board:
            result += ('  |     ' * (self.width + 1)) + '\n'
            result += chr(row_count)
            row_count += 1
            result += ' | '
            count += 3
            for cell in row:
                if not cell:
                    result += "     "
                    count += 5
                else:
                    result += cell.as_colored()
                    count += len(str(cell))
                result += ' | '
                count += 3
            result += '\n'
            result += ('  |     ' * (self.width + 1)) + '\n'
            result += ('-' * count) + '\n'
            count = 0
        return result


class Player(Character):
    def __init__(self, team, count, direction, board, pos, color):
        super().__init__(board, pos)
        self.team = team
        self.count = count
        self.direction = direction
        self.color = color
        self.max_moves = 3
        self.move(pos, self.direction)

    def move(self, pos: Pos, direction):
        super().move(pos)
        self.direction = direction

    def __str__(self):
        return f"{self.team}{self.count}({self.direction})"


class Robot(Character):
    def __init__(self, board, count, pos, color):
        super().__init__(board, pos)
        self.count = count
        self.move(pos)
        self.max_moves = 3
        self.color = color

    def find_affinity(self, players):
        """
        Find a player that reaches an affinity based on.

        1. distance to player
        2. direction to it from the ball (characters direction_to)
        3. ability for it to kick the player in the direction of the goal
        """
        closest_player = players[0]
        for player in players:
            curr = abs(robot.pos.row + robot.pos.col + player.pos.row +
                       player.pos.col)
            closest = abs(robot.pos.row + robot.pos.col +
                          closest_player.pos.row + closest_player.pos.col)
            if curr < closest:
                closest_player = player

        return closest_player

    def __str__(self):
        return f"  R{self.count} "


class Ball(Character):
    def __init__(self, board, pos, color):
        super().__init__(board, pos)
        self.move(pos)
        self.color = color
        self.max_moves = 10

    def __str__(self):
        return "  B  "


def input_player_pos(players, board):
    for player in players:
        while True:
            new_pos = input(f"{player} POS> ")
            if new_pos == '':
                new_pos = f"{player.pos.row}{player.pos.col}"
            new_dir = input(f"{player} DIR> ")
            if new_dir == '':
                new_dir = player.direction

            if not board.is_legal_move(new_pos):
                print("Bad position, try again")
            else:
                break


def move_when_legal(board, character):
    while True:
        row = random.randrange(1, WIDTH)
        col = random.randrange(1, HEIGHT)
        if board.is_legal_move(character, Pos(row, col)):
            if type(character) is Player:
                character.move(Pos(row, col), 'E')
            else:
                character.move(Pos(row, col))
            break


def robot_kicks_ball(board, robot, ball):
    ball_pos = ball.pos
    move_when_legal(board, ball)
    robot.move(ball_pos)


def random_moves_for_players(players, ball, board):
    for player in players:
        move_when_legal(board, player)
        print(board)
        time.sleep(.5)
        print('\x1bc')

    # move_when_legal(board, ball)



if __name__ == '__main__':
    print('\x1bc')
    board = Board()
    x_players = [Player('X', 1, 'E', board, Pos('C', 2), 'red'),
                 Player('X', 2, 'E', board, Pos('D', 2), 'red'),
                 Player('X', 3, 'E', board, Pos('E', 2), 'red'),
                 Player('X', 4, 'E', board, Pos('F', 2), 'red'),
                 Player('X', 5, 'E', board, Pos('G', 2), 'red')
                 ]
    o_players = [Player('O', 1, 'W', board, Pos('C', 8), 'blue'),
                 Player('O', 2, 'W', board, Pos('D', 8), 'blue'),
                 Player('O', 3, 'W', board, Pos('E', 8), 'blue'),
                 Player('O', 4, 'W', board, Pos('F', 8), 'blue'),
                 Player('O', 5, 'W', board, Pos('G', 8), 'blue')
                 ]

    robots = [
        Robot(board, 1, Pos('A', 1), 'green'),
        Robot(board, 2, Pos('J', 1), 'green'),
        Robot(board, 3, Pos('A', 10), 'green'),
        Robot(board, 4, Pos('J', 10), 'green'),
        Robot(board, 5, Pos('C', 5), 'green'),
        Robot(board, 6, Pos('G', 5), 'green')]

    ball = Ball(board, Pos('E', 5), 'yellow')

    robot_kicks_ball(board, robots[4], ball)
    print(board)
    time.sleep(.5)
    print('\x1bc')
    ball_pos = ball.pos

    while True:

        # try:
        #     print("TEAM X\n")
        #     # input_player_pos(x_players, board)
        #     print(board)

        #     print("TEAM O\n")
        #     # input_player_pos(o_players, board)
        # except EOFError:
        #     break

        random_moves_for_players(x_players, ball, board)
        random_moves_for_players(o_players, ball, board)
        # print("ROBOTS MOVE :O")
        for robot in robots:
            player = robot.find_affinity([ball])
            if robot.pos.is_left(player.pos):
                to_move = Pos(player.pos.row, player.pos.col + 1)
            elif robot.pos.is_right(player.pos):
                to_move = Pos(player.pos.row, player.pos.col - 1)
            elif robot.pos.is_up(player.pos):
                to_move = Pos(player.pos.row + 1, player.pos.col)
            elif robot.pos.is_down(player.pos):
                to_move = Pos(player.pos.row - 1, player.pos.col)

            if not board.is_legal_move(robot, to_move):
                move_when_legal(board, robot)
            else:
                robot.move(to_move)
                if type(player) is Ball:
                    robot_kicks_ball(board, robot, ball)

            print(board)
            time.sleep(.5)
            print('\x1bc')
