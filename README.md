# Super Silly Soccer Standoff

or in Europe: Fun Folly Football Frolicking

# Rules

1. Two teams of 5 players each
2. Six robots
3. One ball
4. Goal is for a team to avoid making a goal. The robots try to get your team to kick the ball.

Robots will chase players to kick the ball to the goal, so players need to avoid both robots and the goal.

Players get points for avoiding robots and not kicking the ball.

Each team takes turns before robots go on a hunt.

# Install

Needs the latest Python and Git. On Debians:

```
sudo apt install git
git clone https://gitlab.com/bolsen80/soccer-standoff.git
cd soccer-standoff
python -m venv venv
source venv/bin/activate
python -m pip install termcolor
python game.py
```

It automatically plays a game with random moves, so it looks dumb, but it shows how far players can move and such. 

Robots have some brains, but the AI is cheap - robots hunt for the ball to kick. Players just look like they are panicking.

# Plan

 - [X] Basic printing of game board
 - [X] Basic character logic and moving
 - [ ] Finding the board borders, finding blockers
 - [ ] Robots shortest-path to players
 - [ ] Robot hunting logic
 - [ ] Game close
 - [ ] Auto-player logic

There is a bit more planning, but this is just to get a basic game in place.



# Strategies

Just is just some thoughts to help rule development.

You can presume some strategies to avoid being picked by the robot:

1. Cluster your team far from the ball
2. Spread and confuse bots. You might lose some points for being nudged by a robot
3. Block the robot to limit the space and force it to choose the other team.
4. Complete avoidance might be hard when you can't move your team too far
