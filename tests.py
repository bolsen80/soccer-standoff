"""
Test Position: consider this board:

   1     2     3      4     5     6
 -----|-----|------|------|----|----|
1|    |     |      |      |    |    |
 |----|-----|------|------|----|-----
2|    |     |      |      |    |    |
 -----|-----|------|------|----|----|
3|    |     |  x   |      |    |    |
 -----|-----|------|------|----|----|
4|    |     |      |      |    |    |
 -----|-----|------|------|----|----|
"""

import sys
from game import Pos, Board, Ball, Player


def test_pos_above():
    assert Pos(3, 3).is_up(Pos(1, 3))
    assert not Pos(3, 3).is_up(Pos(4, 3))
    assert Pos(3, 3).is_up(Pos(2, 5))


def test_pos_below():
    assert not Pos(3, 3).is_down(Pos(1, 3))
    assert Pos(3, 3).is_down(Pos(4, 3))
    assert not Pos(3, 3).is_down(Pos(2, 5))


def test_pos_left():
    assert Pos(3, 3).is_left(Pos(2, 2))
    assert not Pos(3, 3).is_left(Pos(4, 3)) # same col
    assert Pos(3, 3).is_left(Pos(2, 1))


def test_pos_right():
    assert not Pos(3, 3).is_right(Pos(2, 2))
    assert not Pos(3, 3).is_right(Pos(4, 3)) # same col
    assert Pos(3, 3).is_right(Pos(4, 4)) # same col
    assert not Pos(3, 3).is_right(Pos(2, 1))


"""
Same board, testing path search space

   1     2     3      4     5     6
 -----|-----|------|------|----|----|
1|    |     |      |      |    |    |
 -----|-----|------|------|----|----|
2|    | z   |      |      | y  |    |
 -----|-----|------|------|----|----|
3|    |  z  |  x   |      | y  |    |
 -----|-----|------|------|----|----|
4|    |     |      |  y   | y  |    |
 -----|-----|------|------|----|----|
5|    |     |      |      |    | t  |
 -----|-----|------|------|----|----|
6|    |     |      |      |    |    |
 -----|-----|------|------|----|----|
"""

board = Board()
ball = Ball(board, Pos(3, 3), 'red')


def test_path_search_space():
    assert (board.path_search_space(ball, Pos(1, 1)) ==
            ((1, 2), (1, 2)))

    assert (board.path_search_space(ball, Pos(4, 1)) ==
            ((4, 4), (1, 2)))

    assert (board.path_search_space(ball, Pos(3, 1)) ==
            ((3, 3), (1, 2)))

    assert (board.path_search_space(ball, Pos(1, 3)) ==
            ((1, 2), (3, 3)))

    assert (board.path_search_space(ball, Pos(2, 6)) ==
            ((2, 2), (4, 6)))

    # For the next test
    assert (board.path_search_space(ball, Pos(5, 6)) ==
            ((4, 5), (4, 6)))
    assert(board.path_search_space(ball, Pos(1, 1)) ==
           ((1, 2), (1, 2)))


characters = [
    Player('X', 1, 'E', board, Pos(2, 2), 'green'),
    Player('X', 2, 'E', board, Pos(3, 2), 'green'),

    Player('O', 3, 'E', board, Pos(4, 4), 'green'),
    Player('O', 4, 'E', board, Pos(2, 5), 'green'),
    Player('O', 5, 'E', board, Pos(3, 5), 'green'),
    Player('O', 6, 'E', board, Pos(4, 5), 'green')
]


# Now see if a character crosses another
def test_path_will_cross_character_left_right():
    print()
    print(board)
    result = str(board.path_will_cross_character(ball, Pos(5, 6), characters))
    to_match = str(characters[2]) # (O3)
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(4, 6), characters))
    to_match = str(characters[2])
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(3, 6), characters))
    to_match = str(characters[4]) # (O5)
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(2, 6), characters))
    to_match = str(characters[3]) # (O4)
    assert result == to_match


    # To the left

    result = str(board.path_will_cross_character(ball, Pos(1, 1), characters))
    to_match = str(characters[0]) # (X1)
    assert result == to_match


    result = str(board.path_will_cross_character(ball, Pos(2, 1), characters))
    to_match = str(characters[0])
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(3, 1), characters))
    to_match = str(characters[1]) # (X2)
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(3, 1), characters))
    to_match = str(characters[1])
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(3, 1), characters))
    to_match = str(characters[1])
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(4, 1), characters))
    to_match = 'None'
    assert result == to_match


"""
Now we test a different board, but up down oriented.

   1     2     3      4     5     6
 -----|-----|------|------|----|----|
1|    |     |      |      |    |    |
 -----|-----|------|------|----|----|
2|    | z   |      |      |    |    |
 -----|-----|------|------|----|----|
3|    |  z  |  x   |      |    |    |
 -----|-----|------|------|----|----|
4|    |     |  y   |      | y  |    |
 -----|-----|------|------|----|----|
5|    |     |  y   |  y   |    |    |
 -----|-----|------|------|----|----|
6|    |     |      |  t   |    |    |
 -----|-----|------|------|----|----|
"""

def test_path_will_cross_character_up_down():
    board = Board()
    characters = [
    Player('X', 1, 'E', board, Pos(2, 2), 'green'),
    Player('X', 2, 'E', board, Pos(3, 2), 'green'),

    Player('O', 3, 'E', board, Pos(4, 3), 'green'),
    Player('O', 4, 'E', board, Pos(4, 5), 'green'),
    Player('O', 5, 'E', board, Pos(5, 3), 'green'),
    Player('O', 6, 'E', board, Pos(5, 4), 'green')
    ]
    ball = Ball(board, Pos(3, 3), 'red')

    print(board)

    result = str(board.path_will_cross_character(ball, Pos(6, 1), characters))
    to_match = 'None'
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(6, 2), characters))
    to_match = 'None'
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(6, 3), characters))
    to_match = str(characters[2]) # (O3)
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(6, 4), characters))
    to_match = str(characters[5]) # (O6)
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(6, 5), characters))
    to_match = str(characters[3]) # (O4)
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(6, 6), characters))
    to_match = str(characters[3])
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(6, 7), characters))
    to_match = str(characters[3])
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(6, 8), characters))
    to_match = str(characters[3])
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(6, 9), characters))
    to_match = str(characters[3])
    assert result == to_match

    result = str(board.path_will_cross_character(ball, Pos(6, 10), characters))
    to_match = str(characters[3])
    assert result == to_match


"""
Now we test a lot of clustered players going up

   1     2     3      4     5     6
 -----|-----|------|------|----|----|
1|    |     |      |      |    |    |
 -----|-----|------|------|----|----|
2|    |     |      |      |    |    |
 -----|-----|------|------|----|----|
3|    |     |  t3  |  t2  |t1  |    |
 -----|-----|------|------|----|----|
4|    | t4  |  y   |  z   | y  |    |
 -----|-----|------|------|----|----|
5|    |     |  y   |      | t5 |    |
 -----|-----|------|------|----|----|
6|    |     |  x   |  y   | z  |    |
 -----|-----|------|------|----|----|
"""

def test_path_will_cross_character_clustered():
    board = Board()

    characters = [
    Player('Z', 1, 'E', board, Pos(4, 4), 'green'),
    Player('Z', 2, 'E', board, Pos(6, 6), 'green'),

    Player('Y', 3, 'E', board, Pos(4, 3), 'green'),
    Player('Y', 4, 'E', board, Pos(4, 5), 'green'),
    Player('Y', 5, 'E', board, Pos(5, 3), 'green'),
    Player('Y', 6, 'E', board, Pos(6, 5), 'green')
    ]
    ball = Ball(board, Pos(6, 3), 'red')

    # t1
    result = str(board.path_will_cross_character(ball, Pos(3, 5), characters))
    to_match = str(characters[0]) # Z1
    assert result == to_match

    #t2
    result = str(board.path_will_cross_character(ball, Pos(3, 4), characters))
    to_match = str(characters[0]) # Z1
    assert result == to_match

    #t3
    result = str(board.path_will_cross_character(ball, Pos(3, 3), characters))
    to_match = str(characters[4]) # Y5
    assert result == to_match

    #t4
    result = str(board.path_will_cross_character(ball, Pos(4, 3), characters))
    to_match = str(characters[4]) # Y5
    assert result == to_match

    #t5
    result = str(board.path_will_cross_character(ball, Pos(5, 5), characters))
    to_match = 'None'
    assert result == to_match


"""
Now we test a lot of clustered players going up

   1     2     3      4     5     6
 -----|-----|------|------|----|----|
1|    |     |  x   |  y   |z   |    |
 -----|-----|------|------|----|----|
2|    |     |  y   |      |t5  |    |
 -----|-----|------|------|----|----|
3|    | t4  |  y   |  z   |y   |    |
 -----|-----|------|------|----|----|
4|    |     | t3   |      |t1  |    |
 -----|-----|------|------|----|----|
5|    |     |      |      |t2  |    |
 -----|-----|------|------|----|----|
6|    |     |      |      |    |    |
 -----|-----|------|------|----|----|
"""

def test_path_will_cross_character_clustered_down():
    board = Board()

    characters = [
    Player('Z', 1, 'E', board, Pos(1, 5), 'green'),
    Player('Z', 2, 'E', board, Pos(3, 4), 'green'),

    Player('Y', 3, 'E', board, Pos(1, 4), 'green'),
    Player('Y', 4, 'E', board, Pos(2, 3), 'green'),
    Player('Y', 5, 'E', board, Pos(3, 3), 'green'),
    Player('Y', 6, 'E', board, Pos(3, 5), 'green')
    ]
    ball = Ball(board, Pos(1, 3), 'red')
    print()
    print(board)

    # t1
    result = str(board.path_will_cross_character(ball, Pos(3, 5), characters))
    to_match = str(characters[1]) # Z2
    assert result == to_match

    #t2
    result = str(board.path_will_cross_character(ball, Pos(3, 4), characters))
    to_match = str(characters[1]) # Z2
    assert result == to_match

    #t3
    result = str(board.path_will_cross_character(ball, Pos(3, 3), characters))
    to_match = str(characters[3]) # Y4
    assert result == to_match

    #t4
    result = str(board.path_will_cross_character(ball, Pos(4, 3), characters))
    to_match = str(characters[3]) # Y4
    assert result == to_match

    #t5
    result = str(board.path_will_cross_character(ball, Pos(5, 5), characters))
    to_match = str(characters[1]) # Z2 (this one is really awkward)
    assert result == to_match
